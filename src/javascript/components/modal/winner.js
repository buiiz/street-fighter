import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = `${fighter.name} wins!`;
  const bodyElement = createElement({ tagName: 'p', className: 'modal-body' });
  bodyElement.textContent = 'See you in the next fight!';
  const onClose = () => window.location.reload();
  showModal({ title, bodyElement, onClose });
}
