import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterInfo = createFighterInfo(fighter);
    const fighterImg = createFighterImage(fighter);
    if (position === 'right') {
      fighterImg.classList.add('fighter-preview___img-reverted');
    }

    fighterElement.append(fighterImg, fighterInfo);
  }

  return fighterElement;
}

function createFighterInfo(fighter, position) {
  const name = createElement({ tagName: 'span', className: 'fighter-preview___info-name' });
  name.textContent = fighter.name;

  const health = createElement({ tagName: 'span', className: 'fighter-preview___info-item' });
  health.textContent = `health: ${fighter.health}`;

  const attack = createElement({ tagName: 'span', className: 'fighter-preview___info-item' });
  attack.textContent = `attack: ${fighter.attack}`;

  const defense = createElement({ tagName: 'span', className: 'fighter-preview___info-item' });
  defense.textContent = `defense: ${fighter.defense}`;

  const infoElement = createElement({ tagName: 'div', className: 'fighter-preview___info' });

  infoElement.append(name, health, attack, defense);

  return infoElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
