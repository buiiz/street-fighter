import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const { PlayerOneAttack, PlayerOneBlock, PlayerOneCriticalHitCombination } = controls;
  const { PlayerTwoAttack, PlayerTwoBlock, PlayerTwoCriticalHitCombination } = controls;
  const firstFighterControls = {
    attack: PlayerOneAttack,
    block: PlayerOneBlock,
    criticalHitCombination: PlayerOneCriticalHitCombination,
  };
  const secondFighterControls = {
    attack: PlayerTwoAttack,
    block: PlayerTwoBlock,
    criticalHitCombination: PlayerTwoCriticalHitCombination,
  };
  const firstPlayer = new Player(firstFighter, firstFighterControls);
  const secondPlayer = new Player(secondFighter, secondFighterControls);

  return new Promise((resolve) => {
    const handleKeydown = (e) => {
      if (e.code == firstPlayer.controls.attack && !firstPlayer.isBlock && !secondPlayer.isBlock) {
        secondPlayer.health -= getDamage(firstPlayer, secondPlayer);
      }
      if (e.code == secondPlayer.controls.attack && !firstPlayer.isBlock && !secondPlayer.isBlock) {
        firstPlayer.health -= getDamage(secondPlayer, firstPlayer);
      }
      if (e.code == firstPlayer.controls.block) {
        firstPlayer.isBlock = true;
      }
      if (e.code == secondPlayer.controls.block) {
        secondPlayer.isBlock = true;
      }
      if (firstPlayer.controls.criticalHitCombination.some((code) => e.code == code)) {
        firstPlayer.keydown(e.code);
        if (firstPlayer.isCritical) {
          secondPlayer.health -= 2 * firstPlayer.attack;
          firstPlayer.cooldown();
        }
      }
      if (secondPlayer.controls.criticalHitCombination.some((code) => e.code == code)) {
        secondPlayer.keydown(e.code);
        if (secondPlayer.isCritical) {
          firstPlayer.health -= 2 * secondPlayer.attack;
          secondPlayer.cooldown();
        }
      }

      updateHealthIndicators(firstPlayer, secondPlayer);

      if (firstPlayer.health <= 0) {
        resolve(secondFighter);
      }
      if (secondPlayer.health <= 0) {
        resolve(firstFighter);
      }
    };

    const handleKeyup = (e) => {
      if (e.code == firstPlayer.controls.block) {
        firstPlayer.isBlock = false;
      }
      if (e.code == secondPlayer.controls.block) {
        secondPlayer.isBlock = false;
      }
      if (firstPlayer.controls.criticalHitCombination.some((code) => e.code == code)) {
        firstPlayer.keyup(e.code);
      }
      if (secondPlayer.controls.criticalHitCombination.some((code) => e.code == code)) {
        secondPlayer.keyup(e.code);
      }
    };

    document.addEventListener('keydown', handleKeydown);
    document.addEventListener('keyup', handleKeyup);
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower > blockPower ? hitPower - blockPower : 0;
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomBetween(1, 2);
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomBetween(1, 2);
  const power = fighter.defense * dodgeChance;
  return power;
}

function getRandomBetween(min, max) {
  return Math.random() * (max - min) + min;
}

function updateHealthIndicators(leftPlayer, rightPlayer) {
  const leftIndicator = document.getElementById('left-fighter-indicator');
  const rightIndicator = document.getElementById('right-fighter-indicator');

  const leftHealth = (leftPlayer.health * 100) / leftPlayer.initialHealth;
  const rightHealth = (rightPlayer.health * 100) / rightPlayer.initialHealth;

  leftIndicator.style.width = `${leftHealth > 0 ? leftHealth : 0}%`;
  rightIndicator.style.width = `${rightHealth > 0 ? rightHealth : 0}%`;
}

class Player {
  constructor(fighter, controls) {
    this.name = fighter.name;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.health = fighter.health;
    this.initialHealth = fighter.health;
    this.controls = { ...controls };
    this.pressedKeys = new Set();
    this.isBlock = false;
    this.isCooldown = false;
  }

  keydown(code) {
    this.pressedKeys.add(code);
  }

  keyup(code) {
    this.pressedKeys.delete(code);
  }

  get isCritical() {
    return this.pressedKeys.size === 3 && this.isCooldown === false;
  }

  cooldown() {
    this.isCooldown = true;
    setTimeout(() => (this.isCooldown = false), 10000);
  }
}
